# char_detector

Using a scratch Neural Network, to detect uppercase letters from a adjusted bitmap.

Please visit the wiki page for more info on running char_detector.py.

### Versions used in writing
---

PYTHON=3.6

NumPy=1.14