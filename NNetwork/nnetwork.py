"""
 @ File Name: nn_medium.py
 @ Created On: 2019.3.4
 @ Orchestrated By: Jason A. Kolodziej
 @ Updated On: Thu Apr 25 2019
 @ Copyright: (c) 2019 by Jason A. Kolodziej. All Rights Reserved.
 
 @ Licensing:
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 @       Licensed under the Apache License, Version 2.0 (the "License");      @
 @       you may not use this file except in compliance with the License.     @
 @       You may obtain a copy of the License at                              @
 @                                                                            @
 @           http://www.apache.org/licenses/LICENSE-2.0                       @
 @                                                                            @
 @       Unless required by applicable law or agreed to in writing, software  @
 @       distributed under the License is distributed on an "AS IS" BASIS,    @      
 @       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,                        @
 @       either express or implied. See the License for the specific language @
 @       governing permissions and limitations under the License.             @
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 
"""

import sys
import numpy as np
import argparse
import csv
import inspect

# pad an existing list to a certain size with content
def pad(l:list, content, width: int):
    l.extend([content] * (width - len(l)))
    return l

def open_training(file:str = 'ibm_chars.csv'):
    perfect_font = {}
    # generate a dictionary holding the perfect input and the expected
    # output associated with the input
    # use a "letter" for the key that the information belongs to
    with open(file, newline='') as csvfile:
        reader = csv.DictReader(csvfile, restkey="Binary")
        for row in reader:
            # convert the list of strings to list of integers
            # from CSV file
            binary = [ int(x) for x in row['Binary'] ]
            binary = pad(binary, 0 , 126)
            binary = [binary] # 1 X 126
            # binary = pad([], binary, 26) # 26 X 126
            # convert the normal list into a numpy array
            # for further computation
            # binary = [binary]s
            perfect_font[row['char']] = binary # (1 X 126 , 1 X 26)
        # debug the dictionary of font definitions we have    
        # print(perfect_font)
    return perfect_font

# activation functions and their derivative
def log_sigmoid(x):
    return 1/(1+np.exp(-x))

def log_sigmoid_d(z):
    return z*(1-z)

def tanh_sigmoid(z):
    return np.tanh(z)

def tanh_sigmoid_d(z):
    return 1-np.tanh(z)**2

# loss function and its derivative
def mse(expected_y, y_pred):
    return np.mean(np.power(expected_y-y_pred, 2));

def mse_prime(expected_y, y_pred):
    return 2*(y_pred-expected_y)/expected_y.size;

# nueral network (base class) NeuralLayer
class NeuralLayer:
    def __init__(self):
        self.input = None
        self.output = None

    # computes the output Y of a layer for a given input X
    def forward_propagation(self, input):
        raise NotImplementedError

    # computes dE/dX for a given dE/dY (and update parameters if any)
    def backward_propagation(self, output_error, learning_rate):
        raise NotImplementedError

# fully connected neural network layer [FCNeuralLayer] (inherited from base class)
class FCNeuralLayer(NeuralLayer):
    # input_size = # of input neurons
    # output_size = # of output neurons
    def __init__(self, input_size, output_size, l_bound:float = -0.1, u_bound:float = 0.1):
        self.weights = np.random.uniform(low=l_bound,high=u_bound,size=(input_size,output_size))
        self.bias = np.random.uniform(low=l_bound,high=u_bound,size=(1,output_size)) #np.random.rand(1, output_size) - 0.5

    # returns output for a given input
    def forward_propagation(self, input_data):
        self.input = input_data
        self.output = np.dot(self.input, self.weights) + self.bias
        return self.output

    # computes (delta of error w.r.t the Weight), (delta of Error w.r.t the Bias) 
    # for a given output_error=(delta of Error w.r.t expected y). 
    # Returns input_error=(delta of error w.r.t X).
    def backward_propagation(self, output_error, learning_rate):
        input_error = np.dot(output_error, self.weights.T)
        weights_error = np.dot(self.input.T, output_error)
        # update parameters
        self.weights -= learning_rate * weights_error
        self.bias -= learning_rate * output_error
        return input_error

# activation laver (inherited from base class NeuralLayer)
class ActivationNeuralLayer(NeuralLayer):
    def __init__(self, activation, activation_prime):
        self.activation = activation
        self.activation_prime = activation_prime

    # returns the activated input
    def forward_propagation(self, input_data):
        self.input = input_data
        # Debugging
        # print(self.input)
        self.output = self.activation(self.input)
        return self.output

    # Returns input_error=(delta of error w.r.t X) 
    # for a given output_error=(delta of Error w.r.t expected y).
    # learn rate is not used because no learnable parameters.
    def backward_propagation(self, output_error, learning_rate):
        return self.activation_prime(self.input) * output_error

class NNetwork:
    def __init__(self):
        self.layers = []
        self.loss = None
        self.loss_prime = None

    # add layer to network
    def add(self, layer):
        self.layers.append(layer)

    # set loss to use
    def use(self, loss, loss_prime):
        self.loss = loss
        self.loss_prime = loss_prime

    # predict output for given input
    def predict(self, input_data):
        # sample dimension first
        samples = len(input_data)
        result = []

        # run network over all samples
        for i in range(samples):
            # forward propagation
            output = input_data[i]
            for layer in self.layers:
                output = layer.forward_propagation(output)
            result.append(output)

        return result

    # train the network
    def fit(self, x_train, y_train, epochs, learning_rate, verbose:bool=False):
        # sample dimension first
        samples = len(x_train)
        print(samples)

        # training loop
        for i in range(epochs):
            err = 0
            for j in range(samples):
                # forward propagation
                output = x_train[j]
                for layer in self.layers:
                    output = layer.forward_propagation(output)

                # compute loss (for display purpose only)
                err += self.loss(y_train[j], output)

                # backward propagation
                error = self.loss_prime(y_train[j], output)
                for layer in reversed(self.layers):
                    error = layer.backward_propagation(error, learning_rate)

            # calculate average error on all samples
            if verbose:
                err /= samples
                print('epoch %d/%d   error=%f' % (i+1, epochs, err))
             
    def write_out_weights(self):
        writer = []
        for layer in self.layers:
            print(isinstance(layer,FCNeuralLayer))
            if isinstance(layer,FCNeuralLayer):
                writer.append(layer.weights)
        np.savez("computed_weights.npmem",writer)

    def load_weights(self):
        weights = np.load("computed_weights.npmem.npz",'.npz')['arr_0']


# convolutional neural network layer (inherited from NeuralLayer base class)
# always strides with 1
class ConLayer(NeuralLayer):
    # input_shape = (i,j,d)
    # kernel_shape = (m,n)
    # layer_depth = output depth
    def __init__(self, input_shape, kernel_shape, layer_depth):
        self.input_shape = input_shape;
        self.input_depth = input_shape[2];
        self.kernel_shape = kernel_shape;
        self.layer_depth = layer_depth;
        self.output_shape = (input_shape[0]-kernel_shape[0]+1, input_shape[1]-kernel_shape[1]+1, layer_depth);
        self.weights = np.random.rand(kernel_shape[0], kernel_shape[1], self.input_depth, layer_depth) - 0.5;
        self.bias = np.random.rand(layer_depth) - 0.5;

    # returns output for a given input
    def forward_propagation(self, input):
        self.input = input;
        self.output = np.zeros(self.output_shape);

        for k in range(self.layer_depth):
            for d in range(self.input_depth):
                self.output[:,:,k] += np.correlate(self.input[:,:,d], self.weights[:,:,d,k], mode='valid') + self.bias[k];

        return self.output

    # computes dE/dW, dE/dB for a given output_error=dE/dY. Returns input_error=dE/dX.
    def backward_propagation(self, output_error, learning_rate):
        in_error = np.zeros(self.input_shape);
        dWeights = np.zeros((self.kernel_shape[0], self.kernel_shape[1], self.input_depth, self.layer_depth));
        dBias = np.zeros(self.layer_depth);

        for k in range(self.layer_depth):
            for d in range(self.input_depth):
                in_error[:,:,d] += np.convolve(output_error[:,:,k], self.weights[:,:,d,k], mode='full');
                dWeights[:,:,d,k] = np.correlate(self.input[:,:,d], output_error[:,:,k], mode='valid');
            dBias[k] = self.layer_depth * np.sum(output_error[:,:,k]);

        self.weights -= learning_rate*dWeights;
        self.bias -= learning_rate*dBias;
        return in_error

