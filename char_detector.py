"""
    #### Jason Kolodziej
    #### 625001848
    #### CSCE 420 -500
    #### Dr. Daugherity
 @ File Name: char_dector.py
 @ Created On: 2019.3.4
 @ Orchestrated By: Jason A. Kolodziej
 @ Updated On: Thu Apr 25 2019
 @ Copyright: (c) 2019 by Jason A. Kolodziej. All Rights Reserved.
 
 @ Licensing:
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 @       Licensed under the Apache License, Version 2.0 (the "License");      @
 @       you may not use this file except in compliance with the License.     @
 @       You may obtain a copy of the License at                              @
 @                                                                            @
 @           http://www.apache.org/licenses/LICENSE-2.0                       @
 @                                                                            @
 @       Unless required by applicable law or agreed to in writing, software  @
 @       distributed under the License is distributed on an "AS IS" BASIS,    @      
 @       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,                        @
 @       either express or implied. See the License for the specific language @
 @       governing permissions and limitations under the License.             @
 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 
"""

import sys
import numpy as np
from sys import argv
import csv
import inspect
from string import ascii_uppercase

# pad an existing list to a certain size with content
def pad(l:list, content, width: int):
    l.extend([content] * (width - len(l)))
    return l

def open_training(file:str = 'ibm_chars.csv'):
    perfect_font = {}
    # generate a dictionary holding the perfect input and the expected
    # output associated with the input
    # use a "letter" for the key that the information belongs to
    with open(file, newline='') as csvfile:
        reader = csv.DictReader(csvfile, restkey="Binary")
        for row in reader:
            # convert the list of strings to list of integers
            # from CSV file
            binary = [ int(x) for x in row['Binary'] ]
            binary = pad(binary, 0 , 126)
            binary = [binary] # 1 X 126
            # binary = pad([], binary, 26) # 26 X 126
            # convert the normal list into a numpy array
            # for further computation
            # binary = [binary]s
            perfect_font[row['char']] = binary # (1 X 126 , 1 X 26)
        # debug the dictionary of font definitions we have    
        # print(perfect_font)
    return perfect_font

# find the corresponding guess
def corresponding_prediction(predictions:list):
    print(predictions)
    indx = np.argmax(predictions)
    confidence = predictions[0][indx] * 100
    print(ascii_uppercase[indx], 'with',confidence, '% percent confidence')
        
        


# activation functions and their derivative
def log_sigmoid(x):
    return 1/(1+np.exp(-x))

def log_sigmoid_d(z):
    return z*(1-z)

def tanh_sigmoid(z):
    return np.tanh(z)

def tanh_sigmoid_d(z):
    return 1-np.tanh(z)**2

# loss function and its derivative
def mse(expected_y, y_pred):
    return np.mean(np.power(expected_y-y_pred, 2));

def mse_prime(expected_y, y_pred):
    return 2*(y_pred-expected_y)/expected_y.size;

# neural network (base class) NeuralLayer
class NeuralLayer:
    def __init__(self):
        self.input = None
        self.output = None

    # computes the output Y of a layer for a given input X
    def forward_propagation(self, input):
        raise NotImplementedError

    # computes dE/dX for a given dE/dY (and update parameters if any)
    def backward_propagation(self, output_error, learning_rate):
        raise NotImplementedError

# fully connected neural network layer [FCNeuralLayer] (inherited from base class)
class FCNeuralLayer(NeuralLayer):
    # input_size = # of input neurons
    # output_size = # of output neurons
    def __init__(self, input_size, output_size, l_bound:float = -0.1, u_bound:float = 0.1):
        self.weights = np.random.uniform(low=l_bound,high=u_bound,size=(input_size,output_size))
        self.bias = np.random.uniform(low=l_bound,high=u_bound,size=(1,output_size)) #np.random.rand(1, output_size) - 0.5

    # returns output for a given input
    def forward_propagation(self, input_data):
        self.input = input_data
        self.output = np.dot(self.input, self.weights) + self.bias
        return self.output

    # computes (delta of error w.r.t the Weight), (delta of Error w.r.t the Bias) 
    # for a given output_error=(delta of Error w.r.t expected y). 
    # Returns input_error=(delta of error w.r.t X).
    def backward_propagation(self, output_error, learning_rate):
        input_error = np.dot(output_error, self.weights.T)
        weights_error = np.dot(self.input.T, output_error)
        # update parameters
        self.weights -= learning_rate * weights_error
        self.bias -= learning_rate * output_error
        return input_error

# activation laver (inherited from base class NeuralLayer)
class ActivationNeuralLayer(NeuralLayer):
    def __init__(self, activation, activation_prime):
        self.activation = activation
        self.activation_prime = activation_prime

    # returns the activated input
    def forward_propagation(self, input_data):
        self.input = input_data
        # Debugging
        # print(self.input)
        self.output = self.activation(self.input)
        return self.output

    # Returns input_error=(delta of error w.r.t X) 
    # for a given output_error=(delta of Error w.r.t expected y).
    # learn rate is not used because no learnable parameters.
    def backward_propagation(self, output_error, learning_rate):
        return self.activation_prime(self.input) * output_error

class NNetwork:
    def __init__(self):
        self.layers = []
        self.loss = None
        self.loss_prime = None

    # add layer to network
    def add(self, layer):
        self.layers.append(layer)

    # set loss to use
    def use(self, loss, loss_prime):
        self.loss = loss
        self.loss_prime = loss_prime

    # predict output for given input
    def predict(self, input_data):
        # sample dimension first
        samples = len(input_data)
        result = []

        # run network over all samples
        for i in range(samples):
            # forward propagation
            output = input_data[i]
            for layer in self.layers:
                output = layer.forward_propagation(output)
            result.append(output)

        return result

    # train the network
    def fit(self, x_train, y_train, epochs, learning_rate, verbose:bool=False):
        # sample dimension first
        samples = len(x_train)
        print(samples)

        # training loop
        for i in range(epochs):
            err = 0
            for j in range(samples):
                # forward propagation
                output = x_train[j]
                for layer in self.layers:
                    output = layer.forward_propagation(output)

                # compute loss (for display purpose only)
                err += self.loss(y_train[j], output)

                # backward propagation
                error = self.loss_prime(y_train[j], output)
                for layer in reversed(self.layers):
                    error = layer.backward_propagation(error, learning_rate)

            # calculate average error on all samples
            if verbose:
                err /= samples
                print('epoch %d/%d   error=%f' % (i+1, epochs, err))
             
    def write_out_weights(self):
        writer = []
        for layer in self.layers:
            # print(isinstance(layer,FCNeuralLayer))
            if isinstance(layer,FCNeuralLayer):
                writer.append([layer.weights,layer.bias])
        np.savez("computed_weights.npmem",writer)

    def load_weights(self):
        self.layers.clear()
        weights = np.load("computed_weights.npmem.npz",'.npz')['arr_0']
        # print(weights)
        l0 = FCNeuralLayer(126, 60)
        l0.weights = weights[0][0]
        l0.bias = weights[0][1]
        self.add(l0)
        self.add(ActivationNeuralLayer(tanh_sigmoid,tanh_sigmoid_d)) #tanh,tanh_prime
        l1 = FCNeuralLayer(60, 26)
        l1.weights = weights[1][0]
        l1.bias = weights[1][1]
        self.add(l1)
        self.add(ActivationNeuralLayer(log_sigmoid,tanh_sigmoid_d))


if __name__ == "__main__":
    net = NNetwork()
    consult = []
    argv_list = argv[1:]
    # print(font_training.get('A'))
    #exit(0)
    if not( "--consult" in argv_list or "--train" in argv_list) or "--help" in argv_list:
        print('No action requested, add --consult or --train')
        print('--consult : will ask for string of binary to compare [ ex: \'1000101...\' ]')
        print('--train : will ask for a current working directory path of the file, defaulted to ibm_chars.csv')
        print('--verbose : will print errors rate in verbose')
        exit(-255)
    if "--consult" in argv_list:
        net.load_weights()
        user = input('Please enter the binary (126 bits): ')
        consult = list(user)
        consult = list(map(int,consult))
        consult = [consult]
        x_test = np.array(consult)
        out = net.predict(x_test)
        ## NEEDED
        corresponding_prediction(out[0])
        print(out)
    else:
        verbose = False
        if "--verbose" in argv_list:
            verbose = True
        # default train case
        user = input('Please enter the name of the train file in the (CWD): ')
        if user is "":
            print('Using standard train data...')
            font_training = open_training()
        else:
            font_training = open_training(user)
        x_train = []
        y_train = []
        y_train = np.zeros((26,1,26),int)
        i = 0
        for key,val in font_training.items():
            x_train.append(val)
            y_train[i][0][i] = 1
            i+=1
        x_train = np.array(x_train)
        y_train = np.array(y_train)
        net.add(FCNeuralLayer(126, 60))
        net.add(ActivationNeuralLayer(tanh_sigmoid,tanh_sigmoid_d)) #tanh,tanh_prime
        net.add(FCNeuralLayer(60, 26))
        net.add(ActivationNeuralLayer(log_sigmoid,tanh_sigmoid_d))
        net.use(mse, mse_prime)
        net.fit(x_train, y_train, 17000, 0.1,verbose)
        print('saving weights and biases calculated...')
        net.write_out_weights()
        print('bye.')