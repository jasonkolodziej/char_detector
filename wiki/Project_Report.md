
**Neural Network Project**
===
# Table of Contents
  * [Problem to Solve](#markdown-header-problem-to-solve)
  * [Restrictions Limitations](#markdown-header-restrictions-limitations)
  * [Approach](#markdown-header-approach)
  * [Sample Run](#markdown-header-sample-run)
  * [Results](#markdown-header-results)
  * [Conclusions](#markdown-header-conclusion)
  * [Project Extension](#markdown-header-project-extension)
  * [Program Instructions](#markdown-header-program-instructions)
  * [Commented Code](#markdown-header-commented-code)
  * [Bibliography](#markdown-header-bibliography)

## Problem to Solve
---
In the artificial intelligence world, we use Neural Networks to seek unseen relationships on unseen data.
Thus, making model(s) so that we can generalize and predict on unseen data.
This is useful because many relationships (in life) between inputs and outputs are non-linear or complex. 

Artificial Neural Networks can calculate these non-linear relationships while not imposing any restrictions on the input variables (their distribution). 

**The current problem for this project is to declare a specific character from a given font (binary pattern).**

Let us pretend that a font file was given to use to write some text. The file however was messed up during compression and transmission. The issue here is we are not able to properly make out the characters when we use the font. Even if the character is unreadable from the human eye, we could utilize a Neural Network to help us resolve which character is pulled from the font file.

## Restrictions Limitations
---
For the output layer we should use the logistic sigmoid activation function. All other layers with in the neural network should use the hyperbolic tangent sigmoid activation function.
Gather the accuracy after each back-propagation pass and epoch (pass number) to show the learning with a plot graph. The Neural Network's should predict with ***90% accuracy***. After training we should write the weights to a file for future testing.

## Approach
---
After downloading the [IBM EGA](https://farsil.github.io/ibmfonts/) font, I utilized the [candela](#c) source code to rip out the characters needed for this project. I then wrote a small C++ function to convert the hexadecimal code into binary. Afterwards, I wrote each character's binary to a file in CSV format for future use (in training.)

Below shows my approach (graphically), of setting up the neural network. Starting off in C++ I attempted to make a Neural Network class. In which vectors, structures, C++STL, were all used to calculate the data through the network. At the point of testing, I ran into an issue sending my inputs across the network. Verbose mode indicated that I was recieving 'nan' as a float [type] value. After an afternoon of research and article reading, I decided to go mainstream in using Python 3.6 and the numPy library to handle all of my calculations. Afterwards, I would utilize [cython](#cy) to boil down my python code into compiled C code to run ~300% faster.********

Moving into python it was very easy to generate the class needed for this project. I first started by just writing the function `def:`'s for the code I had already written in C++. I then realized I should start organizing the functions into a network class.

After writing, I did some testing and was able to train the network to a letter.

When moving to the whole set of characters `A-Z` (the letters we will be recognizing in this network/project) the network stopped displaying a good prediction. 

**Remember: this was after sending all characters through a epoch cycle of 10,000.** 

Testing the random letter `'B'` presented a `.99` prediction in the `26th` position in the `1X26` prediction output layer. `This means (based on my construction) that the network believes my input [ B ] was a "Z"`. 

I learned that the `biases` were not implemented like they should have been so I decided to write my class with a different design.

The table below shows the **FINAL** approach...

Class Type| Inherited | Functionality
--- | --- | --- | ---
NeuralLayer | **NO** | **Serves strictly as a base class**
FCNeuralLayer | YES | Used on `Fully Connected` neurons (contains the *weights* & *biases*) used to prepare the input before sending it into the activation function.
ActivationNeuralLayer | YES | Neuron that is in charge of processing the activation on of input presented by the `FCNeurallayer`..
NNetwork | **NO** | This class is what should be used by the programmer to generate a neural network on data.

### Figure 1. Diagrammed Approach of the Neural Network
![alt text](nn.svg "Figure 1. Diagramed Approach")

##Sample Run

After training the network, I tested a couple "perfect" characters `B` and `Q`, respectively their outputs generated a >90% confidence of the letters.
```
epoch 14961/15000   error=0.003201
epoch 14962/15000   error=0.003201
epoch 14963/15000   error=0.003201
epoch 14964/15000   error=0.003200
epoch 14965/15000   error=0.003200
epoch 14966/15000   error=0.003200
epoch 14967/15000   error=0.003200
epoch 14968/15000   error=0.003200
epoch 14969/15000   error=0.003200
epoch 14970/15000   error=0.003200
epoch 14971/15000   error=0.003200
epoch 14972/15000   error=0.003200
epoch 14973/15000   error=0.003200
epoch 14974/15000   error=0.003200
epoch 14975/15000   error=0.003200
epoch 14976/15000   error=0.003200
epoch 14977/15000   error=0.003200
epoch 14978/15000   error=0.003200
epoch 14979/15000   error=0.003200
epoch 14980/15000   error=0.003200
epoch 14981/15000   error=0.003200
epoch 14982/15000   error=0.003200
epoch 14983/15000   error=0.003200
epoch 14984/15000   error=0.003200
epoch 14985/15000   error=0.003200
epoch 14986/15000   error=0.003200
epoch 14987/15000   error=0.003200
epoch 14988/15000   error=0.003199
epoch 14989/15000   error=0.003199
epoch 14990/15000   error=0.003199
epoch 14991/15000   error=0.003199
epoch 14992/15000   error=0.003199
epoch 14993/15000   error=0.003199
epoch 14994/15000   error=0.003199
epoch 14995/15000   error=0.003199
epoch 14996/15000   error=0.003199
epoch 14997/15000   error=0.003199
epoch 14998/15000   error=0.003199
epoch 14999/15000   error=0.003199
epoch 15000/15000   error=0.003199
[array([[3.39774614e-03, 9.38750089e-01, 5.34365199e-03, 3.63071717e-02,
        2.21078237e-02, 3.63595537e-04, 1.21969243e-02, 4.58003888e-03,
        6.74039005e-04, 5.56863574e-03, 1.48137097e-03, 1.62008324e-02,
        4.31748334e-04, 1.63196842e-03, 1.01270415e-02, 3.88503480e-02,
        1.90305686e-03, 5.09301981e-02, 1.85887152e-02, 4.82272400e-04,
        7.86456087e-03, 4.85534952e-03, 5.17900420e-04, 1.47710516e-03,
        6.90204105e-03, 2.82741071e-02]])]
[array([[1.72397691e-02, 4.74135678e-03, 1.17778147e-02, 2.63028371e-03,
        1.45532631e-02, 8.62478623e-04, 2.61257278e-02, 1.27866309e-02,
        2.58129736e-04, 7.27922336e-03, 7.50537569e-04, 7.62526264e-05,
        6.58896017e-03, 1.00687343e-02, 2.78847400e-02, 1.14713661e-02,
        9.58470021e-01, 9.78081239e-03, 6.97280013e-03, 5.79022513e-03,
        1.56619412e-02, 1.85371193e-02, 1.87384234e-02, 6.71014000e-04,
        1.03471169e-02, 9.38564595e-04]])]
(py_project) Jasons-MacBook-Pro:py_project jasonkolodziej$ 
```

## Result Analysis

After various bugs from, writing out to file and reading into the program. Collecting the `biases & weights` over 17,000 epochs. I was able to successfully train the neural network. Below are some screenshots of me performing a test on `Y` with 5 bits changed against the neural network.

![Altered Y](altered_y.png)

As you can see there is a 96 % confidence that the neural network things this a `Y`.

## Conclusions

```
altered Y:
110000111100001111011001101100110001111000011011000110110001100000100100011000111101000110011101111011000011100000000000000000
```
```
original Y:
110000111100001111000011011001100011110000011000000110000001100000111100000000000000000000000000000000000000000000000000000000
```

Above is the binary of `Y`, where the altered state was the last point before the neural network decided to believe my `Y` was a `T`.
```
(py_project) Jasons-MacBook-Pro:py_project jasonkolodziej$ python char_dector.py --consult
Please enter the binary (126 bits): 110000111100001111011001101100110001111000011011000110110001100000100100011000111101000110011101111011000011100000000000000000
[[5.07441594e-04 2.93399248e-03 5.14667513e-03 8.36957844e-04
  5.11293194e-02 6.84002215e-03 3.63767542e-03 1.93027085e-03
  3.34323467e-04 4.88300148e-03 1.93374158e-02 1.34104929e-03
  1.53389646e-02 8.60688435e-02 3.83550094e-04 6.64301542e-04
  6.16177445e-02 2.63249219e-03 1.31443348e-02 6.62628467e-01
  3.11840074e-03 6.49352808e-02 1.70236911e-01 3.89695000e-03
  3.51945976e-01 4.57822863e-03]]
T with 66.26284668221432 % percent confidence
[array([[5.07441594e-04, 2.93399248e-03, 5.14667513e-03, 8.36957844e-04,
        5.11293194e-02, 6.84002215e-03, 3.63767542e-03, 1.93027085e-03,
        3.34323467e-04, 4.88300148e-03, 1.93374158e-02, 1.34104929e-03,
        1.53389646e-02, 8.60688435e-02, 3.83550094e-04, 6.64301542e-04,
        6.16177445e-02, 2.63249219e-03, 1.31443348e-02, 6.62628467e-01,
        3.11840074e-03, 6.49352808e-02, 1.70236911e-01, 3.89695000e-03,
        3.51945976e-01, 4.57822863e-03]])]
(py_project) Jasons-MacBook-Pro:py_project jasonkolodziej$ 
```
It seems that it took approximately 30 bit changes before, the network produced false confidence. I am pleased with its performance so far, however I feel that it can do even better through substantial training. (But not to over train.) I have mainly learned that linear algebra manipulation and computation plays a big role in getting a good prediction. If you are not careful, your network might mess up on its prediction. (More than what you want it to.)

## Project Extension
For the ending of this project, I would definitely wished to have trained it more with altered data points (if time permitted).
Furthermore, extending this project would be to bring in all the characters in the font file making the network more universal. The neural network will be able to detect every character in the [IBM EGA](https://farsil.github.io/ibmfonts/) font. Even further, add more complex neural layers to help detect multiple font types. Changing the font style might require more complex layer synopses. I would also like to incorporated other layer types of a Neural network into my class.

## Program Instructions

### This program will not run with UNIX Redirection. Please consult the guide below or typing `char_detector.py --help` for more help.

The program `char_detector.py`:

Flag | *Required* | Functionality
--- | --- | ---
--verbose | NO | **default** = False (showing epoch & error)
--consult | Y OR --train flag | string current working directory file where the test data is stored ( i.e. "ibm_chars.manipulated" )
--train | Y OR --consult flag | string current working directory file where the train data is stored, **default = "ibm_chars.csv"**

## Commented Code
---
This section will contain snippets of code used in the project. Each snippet will contain comments of what is  happening.

```
Class NNetwork:
...
# train the network
    def fit(self, x_train, y_train, epochs, learning_rate, verbose:bool=False):
        # sample dimension first
        samples = len(x_train)
        print(samples)

        # training loop
        for i in range(epochs):
            err = 0
            for j in range(samples):
                # forward propagation
                output = x_train[j]
                for layer in self.layers:
                    output = layer.forward_propagation(output)

                # compute loss (for display purpose only)
                err += self.loss(y_train[j], output)

                # backward propagation
                error = self.loss_prime(y_train[j], output)
                for layer in reversed(self.layers):
                    error = layer.backward_propagation(error, learning_rate)

            # calculate average error on all samples
            if verbose:
                err /= samples
                print('epoch %d/%d   error=%f' % (i+1, epochs, err))
```
```
Class NNetwork:
...
 # predict output for given input
    def predict(self, input_data):
        # sample dimension first
        samples = len(input_data)
        result = []

        # run network over all samples
        for i in range(samples):
            # forward propagation
            output = input_data[i]
            for layer in self.layers:
                output = layer.forward_propagation(output)
            result.append(output)

        return result
```
```
# activation laver (inherited from base class NeuralLayer)
class ActivationNeuralLayer(NeuralLayer):
    def __init__(self, activation, activation_prime):
        self.activation = activation
        self.activation_prime = activation_prime

    # returns the activated input
    def forward_propagation(self, input_data):
        self.input = input_data
        # Debugging
        # print(self.input)
        self.output = self.activation(self.input)
        return self.output

    # Returns input_error=(delta of error w.r.t X) for a given output_error=(delta of Error w.r.t expected y).
    # learn rate is not used because no learnable parameters.
    def backward_propagation(self, output_error, learning_rate):
        return self.activation_prime(self.input) * output_error
```
The Activation functions [Logistic & Hyperbolic Tangent Sigmoid] (with their respective derivatives) are defined below for the `ActivationNeuralNetwork` class above.
```
def log_sigmoid(x):
    return 1/(1+np.exp(-x))

def log_sigmoid_d(z):
    return z*(1-z)

def tanh_sigmoid(z):
    return np.tanh(z)

def tanh_sigmoid_d(z):
    return 1-np.tanh(z)**2
```
```
# fully connected neural network layer [FCNeuralLayer] (inherited from base class)
class FCNeuralLayer(NeuralLayer):
    # input_size = # of input neurons
    # output_size = # of output neurons
    def __init__(self, input_size, output_size, l_bound:float = -0.1, u_bound:float = 0.1):
        self.weights = np.random.uniform(low=l_bound,high=u_bound,size=(input_size,output_size))
        self.bias = np.random.uniform(low=l_bound,high=u_bound,size=(1,output_size)) #np.random.rand(1, output_size) - 0.5

    # returns output for a given input
    def forward_propagation(self, input_data):
        self.input = input_data
        self.output = np.dot(self.input, self.weights) + self.bias
        return self.output

    # computes (delta of error w.r.t the Weight), (delta of Error w.r.t the Bias) 
    # for a given output_error=(delta of Error w.r.t expected y). 
    # Returns input_error=(delta of error w.r.t X).
    def backward_propagation(self, output_error, learning_rate):
        input_error = np.dot(output_error, self.weights.T)
        weights_error = np.dot(self.input.T, output_error)
        # update parameters
        self.weights -= learning_rate * weights_error
        self.bias -= learning_rate * output_error
        return input_error

```
## Bibliography
Farsil.github.io. (2019). IBM Fonts by farsil. [online] Available at: https://farsil.github.io/ibmfonts/ [Accessed 25 Apr. 2019].

<a name="bib"></a> Github.com. (2019). candela. [online] Available at: https://github.com/mpercy/candela/ [Accessed 20 Apr. 2019].

Stackoverflow.com. (2019). Python argparse: Make at least one argument required.  Available at:https://stackoverflow.com/questions/6722936/python-argparse-make-at-least-one-argument-required [Accessed 25 Apr. 2019].

### Other various websites:

[Testing a neural network solution
] : https://medium.com/the-test-sheep/testing-a-neural-network-solution-2a0c0b6977dd
